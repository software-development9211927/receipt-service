/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author Pond
 */
public class testCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
        Customer cus1 = new Customer(-1,"pond","0990064691");
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
        Customer delCus = cs.getByTel("0123456789");
        delCus.setTel("0000000000");
        cs.update(delCus);
        System.out.println("Updated");
//        cs.delete(delCus);
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
        
    }
}
